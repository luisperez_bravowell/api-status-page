# api-status-page

## Project setup

```
npm install -g @vue/cli

with this CLI we can easy setup a Vue.js environment
```

### Compiles and hot-reloads for development
```
npm run serve

this will build a local server and provide you with the url
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Documentation
```
https://v3.vuejs.org/guide/installation.html
https://cli.vuejs.org/
https://apollo.vuejs.org/guide/installation.html
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
